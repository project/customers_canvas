<?php

/**
 * @file
 * Customer's Canvas Builder Template.
 *
 * Variables that are available in this template:
 *  - $owner - The owner of the canvas that is being modified.
 *  - $entity - The entity that referenced this builder.
 *  - $finish_form - The form that is submitted with results.
 */
?>

<div class="customers_canvas" id="cc_wrapper">
  <iframe id="editorFrame" width="100%" height="800px"></iframe>
  <?php print drupal_render($finish_form); ?>
</div>
<!-- /#cc_wrapper -->
